## Instalar o ambiente de desenvolvimento

* `npm install -g gulp`
* `npm install -g bower`
* `npm install`
* `bower install`

## Executar (ambiente dev, para autoreload)
`gulp serve`

## Gerar o dist
`gulp dist`
