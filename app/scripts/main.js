(function($) {

  $(document).on('click', '#open-menu', function() {
    $('body').addClass('--show-menu');
  });
  $(document).on('click', '#close-menu', function() {
    $('body').removeClass('--show-menu');
  });

  $(document).on('click', '.menu-mobile .navigation__link', function() {
    $('body').removeClass('--show-menu');
  });
  var phoneMask = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
  };
  var phoneOptions = {
    onKeyPress: function(val, e, field, options) {
      field.mask(phoneMask.apply({}, arguments), options);
    }
  };

  $('.fone').mask(phoneMask, phoneOptions);
  $('.cep').mask('00000-000');
  $('.cnpj').mask('00.000.000/0000-00', {reverse: true});

  var doStylized = function(el) {
    var self = $(el);
    var value;
    if (el.value) {
      value = self.find('option[value='+ el.value + ']').text();
    } else {
      value = self.attr('placeholder') || 'Selecione';
    }
    var fg = self.closest('.select-stylized');
    fg.find('.selected').remove();
    $('<span>')
      .addClass('selected')
      .appendTo(fg)
      .text(value);
  };

  $(document).on('change', '.select-stylized .form-control', function() {
    doStylized(this);
  });
  $('.select-stylized .form-control').each(function(index, el) {
    doStylized(el);
  });

  $(document).on('click', '#btn-modal-tutorial', function() {
    $('#tutorial').modal('toggle');
  });


  var validateForm = function(form, validOnlyTouched) {
    var $form = $(form);
    var valid = true;
    $form.find('.form-control')
      .each(function(index, el) {
        if (!$(el).data('touched') && validOnlyTouched) { return; }
        if (!el.value) {
          $(el)
            .closest('.form-group')
            .addClass('has-error');
          valid = false;
        } else {
          $(el)
            .closest('.form-group')
            .removeClass('has-error');
        }
      });
    return valid;
  };

  $('#cadastro-form')
    .on('submit', function(e) {
      if (!validateForm(this)) {
        e.preventDefault();
      }
    })
    .on('input', function(e) {
      $(e.target).data('touched', true);
      validateForm(this, true);
    });

  $('#tutorial').on('hidden.bs.modal', function (e) {
    if (player) {
      player.stopVideo();
    }
  });
  $('#tutorial').on('show.bs.modal', function (e) {
    if (player) {
      player.playVideo();
    }
  });

  var $body = $('body');
  var bodyHeight = function() {
    var wH = window.innerHeight;
    if ($body.hasClass('secao-login')) {
      $body.css('height', wH + 'px');
    }
  };
  bodyHeight();
  $(document).on('resize', function() {
    bodyHeight();
  });

  $(document).on('click', '#forgot-password', function(e) {
    e.preventDefault();
    $('.l-body .l-box').toggleClass('l-box--forgot');
  });

})(jQuery);
